---
title: "Maï Phan-Van"
---

Peindre, dessiner comme on écrit, avec le coeur. Et rien qu'avec le coeur, au plus près de l'émotion, fuyant l'emphase et la performance. Parce-qu'image et texte ne doivent pas se faire d'ombre, mais se servir l'un l'autre pour offrir au monde un ballet inspirant.
